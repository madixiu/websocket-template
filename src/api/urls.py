from django.urls import path, re_path


from .views import PostgrestTest

app_name = 'api'
urlpatterns = [
    # path("", InboxView.as_view()),
    # re_path(r"^(?P<username>[\w.@+-]+)", ThreadView.as_view()),
    re_path(r"^postgrest/$",PostgrestTest)
]
