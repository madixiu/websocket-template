import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from .models import Thread, ChatMessage
from .ApiGet import publicApi,realTimePublicApi
from .randomGen import get_number


class ChatConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        await self.send({
            "type": "websocket.accept"
        })
        other_user = self.scope['url_route']['kwargs']['username']
        me = self.scope['user']
        print(other_user ,me)
        # thread_obj = await self.get_thread(me , other_user)
        # await asyncio.sleep(2)
        # print(thread_obj)
        await self.send({
            "type": "websocket.send",
            "text": json.dumps(publicApi(46348559193224090))
        })

        #######################################
        
        # await self.send({
        #     "type": "websocket.send",
        #     "text": "hello world"
        # })
    async def websocket_receive(self,event):
        # when a message is received from the websocket
        print("receive",event)
        front_text = event.get('text', None)
        if front_text is not None:
            loaded_dict_data = json.loads(front_text)
            msg = loaded_dict_data.get('message')
            print(msg)

    async def websocket_disconnect(self,event):
        print("disconnected",event)

    @database_sync_to_async
    def get_thread(self,user,other_username):
        return Thread.objects.get_or_new(user,other_username)[0]

class test(AsyncConsumer):
    async def websocket_connect(self,event):
        # print("connected", event)
        # print(realTimePublicApi())
        await self.send({
            "type": "websocket.accept"
        })
        await self.send({
            "type":"websocket.send",
            "text": json.dumps(realTimePublicApi()),
            
        })

class random(AsyncConsumer):
    async def websocket_connect(self,event):
        await self.send({
            "type": "websocket.accept"
        })
        while(True):
            await self.send({
                "type":"websocket.send",
                "text": str(get_number())
                
            })
            await asyncio.sleep(1)