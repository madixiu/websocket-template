from chat.consumers import ChatConsumer,test,random
from django.urls import re_path
# from django.urls import re_path,path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator,OriginValidator

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    re_path(r"^messages/(?P<username>[\w.@+-]+)/$", ChatConsumer),
                    re_path(r"^user2/$", test),
                    re_path(r"^user3/$",random)


                ]
            )
           
        )
    )
})